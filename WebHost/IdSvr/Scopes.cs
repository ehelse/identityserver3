﻿/*
 * Copyright 2014 Dominick Baier, Brock Allen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using IdentityServer3.Core.Models;

namespace MHealth.IdentityServer.IdSvr
{
    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            return new List<Scope>
            {
                StandardScopes.OpenId,
                StandardScopes.Roles,
                StandardScopes.Profile,
                StandardScopes.Email,
                StandardScopes.OfflineAccess,
                 new Scope
                {
                    Name = "read",
                    DisplayName = "Read data",
                    Type = ScopeType.Resource,
                    Emphasize = false
                },
                 new Scope
                {
                    Name = "fhir_testclient",
                    DisplayName = "Test Client for FHIR Server",
                    Type = ScopeType.Resource,
                    Emphasize = true,
                    ScopeSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    }
                },
                new Scope
                {
                    Name = "write",
                    DisplayName = "Write data",
                    Type = ScopeType.Resource,
                    Emphasize = true,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("test", alwaysInclude: true)
                    }
                },
                new Scope
                {
                    Name = "forbidden",
                    DisplayName = "Forbidden scope",
                    Type = ScopeType.Resource,
                    Emphasize = true
                },
                new Scope
                {
                    Name = "mobile.client",
                    DisplayName = "Write observation",
                    Type = ScopeType.Resource,
                    Emphasize = false,
                    ScopeSecrets = new List<Secret>
                    {
                        new Secret("WvTQZ7fKUzBQuxQR".Sha256())
                    }
                },
                new Scope
                {
                    Name = "web.test.client",
                    DisplayName = "Access to all resources for given patient",
                    Type = ScopeType.Resource,
                    Emphasize = false
                }, 
                new Scope
                {
                    Name = "patient/*.*",
                    DisplayName = "Full access to all resources for given patient",
                    Type = ScopeType.Resource,
                    Emphasize = true
                },
                new Scope
                {
                    Name = "patient/*.read",
                    DisplayName = "Read access to all resources for given patient",
                    Type = ScopeType.Resource,
                    Emphasize = false
                },
                new Scope
                {
                    Name = "patient/Observation.write",
                    DisplayName = "Write measurements (observations) for given patient",
                    Type = ScopeType.Resource,
                    Emphasize = false
                },
                new Scope
                {
                    Name = "patient/QuestionnaireResponse.write",
                    DisplayName = "Write questionnaire responses for given patient",
                    Type = ScopeType.Resource,
                    Emphasize = false
                },
                new Scope
                {
                    Name = "patient/Device.write",
                    DisplayName = "Create or change device information",
                    Type = ScopeType.Resource,
                    Emphasize = false
                },
                new Scope
                {
                    Name = "patient/Patient.read",
                    DisplayName = "Read identity and profile of given patient",
                    Type = ScopeType.Resource,
                    Emphasize = false
                }
            };
           
        }
    }
}