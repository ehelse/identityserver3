﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using IdentityModel.Client;


namespace MHealth.CodeflowAppProxy.Controllers
{
    public class CallbackController : ApiController
    {
        [HttpPost]
       // [ActionName("Index")]
        public TokenResponse PostToken()
        {
            var client = new TokenClient(
                Constants.TokenEndpoint,
                "codeclient",
                "secret");

            var queryStringCol = HttpUtility.ParseQueryString(Request.RequestUri.AbsoluteUri);
            

            var code = queryStringCol["code"];
            //var tempState = await GetTempStateAsync();
            //Request.GetOwinContext().Authentication.SignOut("TempState");

            var redirecturi = ConfigurationManager.AppSettings["ClientBaseUri"] + "/callback"; // Should contain the proxy callback uri

            var response = client.RequestAuthorizationCodeAsync(
                code,
                redirecturi).Result;

            Debug.WriteLine(response.IdentityToken);

            return response;
        }

    }
}
