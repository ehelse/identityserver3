﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;

namespace MHealth.CodeflowAppProxy
{
    public class ProxyHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            UriBuilder identityServerUriBuilder = new UriBuilder(Constants.BaseAddress);
           
            var clonedRequest = CloneHttpRequestMessage(request, identityServerUriBuilder.Uri);
            
            if (clonedRequest.Method == HttpMethod.Get)
                clonedRequest.Content = null;

            var cookieCollection = request.Headers.GetCookies();

            HttpClientHandler myHandler = new HttpClientHandler
            {
                CookieContainer = PopulateCookieCollection(cookieCollection)
            };

            HttpResponseMessage response;

            using (myHandler)
            using (var client = new HttpClient(myHandler))
            {
                response =
                    await client.SendAsync(clonedRequest, HttpCompletionOption.ResponseHeadersRead, cancellationToken);
            }

            return response;
        }


        public CookieContainer PopulateCookieCollection(Collection<CookieHeaderValue> inputCookies)
        {

            CookieContainer outputCookies = new CookieContainer();

            foreach (var cookieHeaderValues in inputCookies)
            {
                foreach (var val in cookieHeaderValues.Cookies)
                {
                    var inputCookie = val;

                    if (inputCookie != null)
                    {
                        Uri target = new Uri(Constants.BaseAddress);
                        Cookie cookie = new Cookie(inputCookie.Name, inputCookie.Value) { Domain = target.Host };
                        outputCookies.Add(cookie);
                    }
                }
            }

            return outputCookies;
        }


       public static HttpRequestMessage CloneHttpRequestMessage(HttpRequestMessage req, Uri requestUri = null)
        {
            if (requestUri == null)
                requestUri = req.RequestUri;

           UriBuilder clonedRequestUri = new UriBuilder(req.RequestUri)
           {
               Host = requestUri.Host,
               Port = requestUri.Port
           };

           HttpRequestMessage clone = new HttpRequestMessage(req.Method, clonedRequestUri.Uri);

            // Copy the request's content (via a MemoryStream) into the cloned object
            var ms = new MemoryStream();
            if (req.Content != null)
            {
                ms.Position = 0;
                clone.Content = new StreamContent(ms);

                // Copy the content headers
                if (req.Content.Headers != null)
                    foreach (var h in req.Content.Headers)
                        clone.Content.Headers.Add(h.Key, h.Value);
            }

            clone.Version = req.Version;

            foreach (KeyValuePair<string, object> prop in req.Properties)
                clone.Properties.Add(prop);

            foreach (KeyValuePair<string, IEnumerable<string>> header in req.Headers)
                clone.Headers.TryAddWithoutValidation(header.Key, header.Value);

            return clone;
        }
    }
}