﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace MHealth.ReverseProxy
{
    
    
        /// <summary>
        /// Handler that intercept Client's request and deliver the web site
        /// </summary>

        public class ReverseProxy : IHttpHandler
        {
            /// <summary>
            /// Method calls when client request the server
            /// </summary>
            /// &;lt;param name="context">HTTP context for client</param>

            public void ProcessRequest(HttpContext context)
            {
               //read values from configuration 
                int proxyMode =
                  Convert.ToInt32(ConfigurationManager.AppSettings["ProxyMode"]);
                string remoteWebSite =
                  ConfigurationManager.AppSettings["RemoteWebSite"];
                string remoteUrl;
                if (proxyMode == 0)
                    remoteUrl = ParseURL(context.Request.Url.AbsoluteUri);
                
               
                UriBuilder forwardUriBuilder = new UriBuilder(context.Request.Url.AbsoluteUri);

            //all site 
            remoteUrl =
                remoteWebSite.TrimEnd('/') + forwardUriBuilder.Path + forwardUriBuilder.Query;

            //context.Request.Url.AbsoluteUri.Replace("http://" +
            //context.Request.Url.Host +
            //context.Request.ApplicationPath, remoteWebSite);

            //only one site accepted
            //create the web request to get the remote stream
            HttpWebRequest request =
                  (HttpWebRequest)WebRequest.Create(remoteUrl);
                //TODO : you can add your own credentials system
                //request.Credentials = CredentialCache.DefaultCredentials;

            request.CookieContainer = PopulateCookieCollection(context.Request.Cookies);

                HttpWebResponse response;
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (System.Net.WebException we)
                {
                    //remote url not found, send 404 to client 
                    context.Response.StatusCode = 404;
                    context.Response.StatusDescription = "Not Found";
                    context.Response.Write("<h2>Page not found</h2>");
                    context.Response.End();
                    return;
                }
                Stream receiveStream = response.GetResponseStream();

                if ((response.ContentType.ToLower().IndexOf("html") >= 0)
                  || (response.ContentType.ToLower().IndexOf("javascript") >= 0))
                {
                    //this response is HTML Content, so we must parse it
                    StreamReader readStream =
                      new StreamReader(receiveStream, Encoding.Default);
                    Uri test = new Uri(remoteUrl);
                    string content;
                    if (proxyMode == 0)
                        content = ParseHtmlResponse(readStream.ReadToEnd(),
                          context.Request.ApplicationPath + "/http//" + test.Host);
                    else
                        content = ParseHtmlResponse(readStream.ReadToEnd(),
                          context.Request.ApplicationPath);
                    //write the updated HTML to the client
                    context.Response.Write(content);
                    //close streamsreadStream.Close();
                    response.Close();
                    context.Response.End();
                }
                else
                {
                    //the response is not HTML 
                    byte[] buff = new byte[1024];
                    int bytes = 0;
                    while ((bytes = receiveStream.Read(buff, 0, 1024)) > 0)
                    {
                        //Write the stream directly to the client 
                        context.Response.OutputStream.Write(buff, 0, bytes);
                    }
                    //close streams
                    response.Close();
                    context.Response.End();
                }
            }


            public CookieContainer PopulateCookieCollection(HttpCookieCollection inputCookies)
            {

                CookieContainer outputCookies = new CookieContainer();

                foreach (var key in inputCookies.AllKeys)
                {
                    var inputCookie = inputCookies[key];

                    if (inputCookie != null)
                    {
                    Uri target = new Uri("http://localhost:44333");
                    Cookie chocolateChip = new Cookie(inputCookie.Name, inputCookie.Value) { Domain = target.Host };
                    outputCookies.Add(chocolateChip);
                    }
                }

                return outputCookies;
            }

            /// <summary>
            /// Get the remote URL to call
            /// </summary>
            /// <param name="url">URL get by client</param>
            /// <returns>Remote URL to return to the client</returns>

            public string ParseURL(string url)
            {
                if (url.IndexOf("http/") >= 0)
                {
                    string externalUrl = url.Substring(url.IndexOf("http/"));
                    return externalUrl.Replace("http/", "http://");
                }
                else
                    return url;
            }

            /// <summary>
            /// Parse HTML response for update links and images sources
            /// </summary>
            /// <param name="html">HTML response</param>
            /// <param name="appPath">Path of application for replacement</param>
            /// <returns>HTML updated</returns>
            public string ParseHtmlResponse(string html, string appPath)
            {
                //html = html.Replace("\"/", "\"" + appPath + "/");
                //html = html.Replace("'/", "'" + appPath + "/");
                //html = html.Replace("=/", "=" + appPath + "/");
                return html;
            }
            ///
            /// Specifies whether this instance is reusable by other Http requests
            ///
            public bool IsReusable
            {
                get
                {
                    return true;
                }
            }
        }
    }
