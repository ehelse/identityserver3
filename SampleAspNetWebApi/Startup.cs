﻿using System.Configuration;
using System.IdentityModel.Tokens;
using IdentityServer3.AccessTokenValidation;
using MHealth.CodeflowResourceService;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup("ProxyStartup", typeof(Startup))]

namespace MHealth.CodeflowResourceService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();
            string serverBaseUri = ConfigurationManager.AppSettings["ServerBaseUri"];

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
                {
                    Authority = serverBaseUri,
                    RequiredScopes = new[] { "write" },

                    // client credentials for the introspection endpoint
                    ClientId = "write",
                    ClientSecret = "secret"
                });
            
            app.UseWebApi(WebApiConfig.Register());
        }
    }
}