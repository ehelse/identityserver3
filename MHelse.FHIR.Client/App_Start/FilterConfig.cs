﻿using System.Web.Mvc;

namespace MHelse.IdentityServer3.Example.Client.OWIN
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}