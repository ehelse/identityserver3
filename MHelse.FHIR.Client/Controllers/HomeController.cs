﻿using System.Web.Mvc;

namespace MHelse.IdentityServer3.Example.Client.OWIN.Controllers
{
    public sealed class HomeController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }
    }
}